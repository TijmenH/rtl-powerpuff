import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

// Styles
import 'styles/_reset.scss';
import 'styles/typography.scss';
import 'styles/page.scss';

// Components
import RouterComponent from 'components/Router';

// Store
import reducer from './store/reducers';

/**
  * App entry
  *
  * @author Tijmen Helder
  */
const App = props => {
  const store = createStore(reducer);

  return (
    <Provider store={store}>
      <main>
        <RouterComponent />
      </main>
    </Provider>
  );
};

export default App;
