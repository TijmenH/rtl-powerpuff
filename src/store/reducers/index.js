import { combineReducers } from 'redux';

// Reducers
import showReducer from './shows';
import episodeReducer from './episodes';

const rootReducer = combineReducers({
  show: showReducer,
  episode: episodeReducer
});

export default rootReducer;
