import { SET_EPISODE } from 'store/constants/ActionTypes';

const shows = (state = null, action) => {
  switch(action.type) {
    case SET_EPISODE:
      return action.data
    default:
      return state;
  }
}

export default shows;
