import { SET_SHOW } from 'store/constants/ActionTypes';

const shows = (state = null, action) => {
  switch(action.type) {
    case SET_SHOW:
      return action.data
    default:
      return state;
  }
}

export default shows;
