import * as types from 'store/constants/ActionTypes';

export const setShow = data => {
  return ({
    type: types.SET_SHOW,
    data
  })
};

export const setEpisode = data => ({
  type: types.SET_EPISODE,
  data
});
