import axios from 'axios';

export const axiosGet = async (endpoint, params) => {
  const response = await axios.get(
    `https://api.tvmaze.com/${endpoint}`,
    {
      params
    }
  );
  return response;
};
