import React from 'react';

// Style module
import styles from './flex-content.module.scss';

/**
  * Flex container which renders content side by side
  *
  * @author Tijmen Helder
  */
const FlexContent = props => (
  <div
    className={props.spaceAround ?
      [styles['flex-container'], styles['space-around']].join(' ') :
      styles['flex-container']}
    >
      {props.children}
    </div>
);

export default FlexContent;
