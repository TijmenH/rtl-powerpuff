import React from 'react';

import styles from './list.module.scss';

/**
  * List component
  *
  * @author Tijmen Helder
  */
const List = props => (
  <div className={styles['list']}>
    {props.children}
  </div>
);

export default List;
