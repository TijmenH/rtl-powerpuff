import React from 'react';

// Style modules
import styles from './episode-card.module.scss';

// Components
import H3 from 'components/H3';
import SubTitle from 'components/SubTitle';
import TextItem from 'components/TextItem';

/**
  * Card that renders an episode item
  *
  * @author Tijmen Helder
  */
const EpisodeCard = ({ episode }) => (
  <div className={styles['episode-card']}>
    {episode.image ? (
      <img
        src={episode.image ? episode.image.medium : ''}
        alt={`${episode.name} episode cover`}
      />
    ) : (
      <div className={styles['placeholder']}>
        No image
      </div>
    )}
  <div className={styles['content']}>
    <div className={styles['title']} title={episode.name}>
      <H3>{episode.name}</H3>
    </div>
      <SubTitle>{`S${episode.season}E${episode.number}`}</SubTitle>
      <TextItem>{episode.airdate}</TextItem>
    </div>
  </div>
);

export default EpisodeCard;
