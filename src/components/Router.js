import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import EpisodesDetail from 'pages/EpisodesDetail';
import SeriesDetail from 'pages/ShowDetail';

/**
  * The router component
  *
  * @author Tijmen Helder
  */
const RouterComponent = props => (
  <Router>
    <Switch>
      <Route exact path="/" component={SeriesDetail} />
      <Route path="/shows/:slug/episodes/:id" component={EpisodesDetail} />
    </Switch>
  </Router>
);

export default RouterComponent;
