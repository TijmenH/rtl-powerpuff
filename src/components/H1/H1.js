import React from 'react';

import styles from './h1.module.scss';

/**
  * Heading 1 element
  *
  * @author Tijmen Helder
  */
const H1 = props => (
  <h1 style={styles['h1']}>{props.children}</h1>
);

export default H1;
