import React from 'react';

// Style modules
import styles from './sub-title.module.scss';

/**
  * A subtitle element
  *
  * @author Tijmen Helder
  */
const SubTitle = props => (
  <p className={styles['sub-title']}>{props.children}</p>
);

export default SubTitle;
