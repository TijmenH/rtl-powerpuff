import React from 'react';

// CSS module
import styles from './h3.module.scss';

/**
  * H3 element
  *
  * @author Tijmen Helder
  */
const H3 = props => (
  <h3 className={styles['h3']}>{props.children}</h3>
);

export default H3;
