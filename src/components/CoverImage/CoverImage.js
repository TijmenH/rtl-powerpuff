import React from 'react';

import styles from './cover-image.module.scss';

/**
  * The cover image of a media item
  *
  * @author Tijmen Helder
  */
const CoverImage = props => (
  <img
    className={styles['cover-image']}
    src={props.image ? props.image : null}
    alt={props.alt}
  />
);

export default CoverImage;
