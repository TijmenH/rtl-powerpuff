import React from 'react';

// Style modules
import styles from './text-item.module.scss';

/**
  * A subtitle element
  *
  * @author Tijmen Helder
  */
const TextItem = props => (
  <p className={styles['text-item']}>{props.children}</p>
);

export default TextItem;
