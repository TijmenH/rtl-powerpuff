import React from 'react';

// Style modules
import styles from './summary.module.scss';

/**
  * Summary component renders html summary
  *
  * @author Tijmen Helder
  */
const Summary = props => (
  <div
    className={styles['summary']}
    dangerouslySetInnerHTML={{ __html: props.children }}
  />
);

export default Summary;
