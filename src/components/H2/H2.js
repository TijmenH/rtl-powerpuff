import React from 'react';

// CSS module
import styles from './h2.module.scss';

/**
  * H2 element
  *
  * @author Tijmen Helder
  */
const H2 = props => (
  <h2 className={styles['h2']}>{props.children}</h2>
);

export default H2;
