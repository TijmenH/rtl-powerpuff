import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Utils
import { axiosGet } from 'utils/axiosClient';

// State
import { setShow, setEpisode } from 'store/actions';

// Components
import H1 from 'components/H1';
import FlexContent from 'components/FlexContent';
import Summary from 'components/Summary'
import CoverImage from 'components/CoverImage';

/**
  * The detail page of an episode of a series
  *
  * @author Tijmen Helder
  */
const EpisodesDetail = props => {
  const episode = useSelector(state => state.episode);
  const show = useSelector(state => state.show);
  const dispatch = useDispatch(); 

  const { slug } = props.match.params;
  const { id } = props.match.params;

  useEffect(() => {
    async function fetch() {
      if (!show) {
        // Fetch the show if not existing in state
        const showResponse = await axiosGet(
          'singlesearch/shows',
          { q: slug, embed: 'episodes'}
        );
        dispatch(setShow(showResponse.data));
      }

      // Fetch the episode if not existing in state or the id in URL changed
      if (!episode || id !== episode.id.toString()) {
        const episodeResponse = await axiosGet(`episodes/${id}`, {});
        dispatch(setEpisode(episodeResponse.data));
      }
    }

    fetch();
  }, [dispatch, id, slug, show, episode]);

  if (episode) {
    return (
      <section className='page'>
        <H1>{episode.name}</H1>
        <FlexContent spaceAround>
          <Summary>{episode.summary}</Summary>
          <CoverImage
            image={episode.image ? episode.image.original : null}
            alt={`${episode.name} cover`}
          />
        </FlexContent>
      </section>
    );
  }

  return null;
};

export default EpisodesDetail;
