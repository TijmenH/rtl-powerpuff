import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

// Components
import H1 from 'components/H1';
import H2 from 'components/H2';
import Summary from 'components/Summary';
import List from 'components/List';
import EpisodeCard from 'components/EpisodeCard';
import FlexContent from 'components/FlexContent';
import CoverImage from 'components/CoverImage';

// State
import { setShow } from 'store/actions';

// Utils
import { axiosGet } from 'utils/axiosClient';

/**
  * The detail page of a series
  *
  * @author Tijmen Helder
  */
const SeriesDetail = props => {
  const show = useSelector(state => state.show);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!show) {
      // Fetch the show if not in state
      axiosGet(
        'singlesearch/shows',
        { q: 'the-powerpuff-girls', embed: 'episodes'}
      ).then(response => {
        dispatch(setShow(response.data));
      });
    }
  }, [dispatch, show]);

  if (show) {
    return (
      <section className='page'>
        <div>
          <H1>{show.name}</H1>
          <FlexContent spaceAround>
            <Summary>{show.summary}</Summary>
            <CoverImage
              image={show.image ? show.image.original : null}
              alt={`${show.name} cover`}
            />
          </FlexContent>
        </div>

        <H2>Episodes</H2>
        <List>
          {show._embedded.episodes.map(episode => (
            <Link
              key={episode.id}
              to={`/shows/${show.name.split(' ').join('-').toLowerCase()}/episodes/${episode.id}`}
            >
              <EpisodeCard episode={episode}/>
            </Link>
          ))}
        </List>
      </section>
    );
  }

  return null;
};

export default SeriesDetail;
